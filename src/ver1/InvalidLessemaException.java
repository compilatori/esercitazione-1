package ver1;

public class InvalidLessemaException extends Exception{
    public InvalidLessemaException(int position){
        System.out.println("ERROR at position "+ position);
    }
}
