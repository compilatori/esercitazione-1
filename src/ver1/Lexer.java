package ver1;

import java.io.*;
import java.util.HashMap;


public class Lexer {
	
	private File input;
	private static HashMap<String, Token> stringTable;  // la struttura dati potrebbe essere una hash map
	private int state;
	private RandomAccessFile raf ;
	private int cursor ;


	public Lexer(){
		// la symbol table in questo caso la chiamiamo stringTable
		stringTable = new HashMap<>();
		state = 0;
		stringTable.put("if", new Token("IF"));   // inserimento delle parole chiavi nella stringTable per evitare di scrivere un diagramma di transizione per ciascuna di esse (le parole chiavi verranno "catturate" dal diagramma di transizione e gestite e di conseguenza). IF poteva anche essere associato ad una costante numerica
		stringTable.put("then", new Token("THEN"));
		stringTable.put("else", new Token("ELSE"));
		stringTable.put("for", new Token("FOR"));
		stringTable.put("while", new Token("WHILE"));
		stringTable.put("float", new Token("FLOAT"));
		stringTable.put("int", new Token("INT"));
		stringTable.put("and", new Token("AND"));
		stringTable.put("or", new Token("OR"));


	}
	
	public Boolean initialize(String filePath){
	    // prepara file input per lettura e controlla errori
		input= new File(filePath);
		try {
			raf = new RandomAccessFile(filePath, "r");
			raf.seek(cursor);
			return raf.read() != -1;
		} catch (IOException e) {
			return false;
		}
	} 

	public Token nextToken()throws Exception {

		//Ad ogni chiamata del lexer (nextToken())
		//si resettano tutte le variabili utilizzate
		state = 9;
		String lessema =""; //il lessema riconosciuto
		raf.seek(cursor);
        char c ;
		int lookHead;
		int r;
		
		while(true){
			// legge un carattere da input e lancia eccezione quando incontra EOF per restituire null
			//  per indicare che non ci sono più token
            r= raf.read();
			if(r ==-1){ // controlla se è finito il file
				return null;
			}
			c = (char) r;
			//Lo utilizzo per controllare se il file è terminato DOPO IL CARATTERE CHE STO LEGGENDO
			lookHead =  raf.read();
			cursor++;
			raf.seek(cursor);

			//id
			switch(state){
				case 9:
					if(Character.isLetter(c)){
						state = 10;
						lessema += c;
						// Nel caso in cui il file è terminato ma ho letto qualcosa di valido
						// devo lanciare il token (altrimenti perderei l'ultimo token, troncato per l'EOF) 

						if(lookHead == -1){ // controlla se è finito il file
							return installID(lessema);
						}
					}
					else if(c == '='){
						state = 29;
					}
					else{
						state = 12;
					}
					break;
				case 10:
					if(Character.isLetterOrDigit(c)){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							return installID(lessema);
						break;
					}

					else{
						state = 11;
						retrack();
						return installID(lessema);
					}
				default: break;
			}//end switch
			
			//numeri
			switch(state){
				case 12:
					if(Character.isDigit(c)){
						state = 13;
						lessema += c;

						if(lookHead == -1) // controlla se è finito il file
							return new Token("NUMBER", lessema);
					}
					else{
						state = 35; // Operatori
					}
					break;
                case 13:
					if(Character.isDigit(c)){
						lessema += c;
						if(lookHead == -1){ // controlla se è finito il file
							return new Token("NUMBER", lessema);
						}
						break;
					}
					else if (c == '.'){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						state = 14;
						break;

					}
					else{
						state = 20;
						retrack();
						return new Token("NUMBER", lessema);
					}
				case 14:
					if(Character.isDigit(c)){
						lessema += c;

						if(lookHead == -1){ // controlla se è finito il file
							return new Token("NUMBER", lessema);
						}
						break;
					}
					else{
						state = 15;
					}

				case 15:

					if (c == 'E'){
						lessema += c;
						if(lookHead == -1){ // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						}
						state = 16;
					}
					else{
						state = 21;
						retrack();
						return new Token("NUMBER", lessema);
					}
					break;
				case 16:
					if(Character.isDigit(c)){
						state = 18;
						lessema += c;
						if(lookHead == -1){ // controlla se è finito il file
							return new Token("NUMBER", lessema);
						}
					}
					if (c == '+' || c == '-'){
						state = 17;
						lessema+=c;
					}
					break;
				case 17:
					if(Character.isDigit(c)){
						state = 18;
						lessema += c;
						if(lookHead == -1){ // controlla se è finito il file
							return new Token("NUMBER", lessema);
						}
					}
					else{
						throw new InvalidLessemaException(cursor);
					}
					break;
				case 18:
					if(Character.isDigit(c)){
						lessema += c;
						if(lookHead == -1){ // controlla se è finito il file
							return new Token("NUMBER", lessema);
						}
					}
					else{
						state = 19;
						retrack();
						return new Token("NUMBER", lessema);
					}
					break;
			}

			//==
			switch (state){
				case 29:
					if (c == '='){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						state = 30;
					}
					break;
				case 30:
					if (c == '='){
						state = 31;
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
					}
					else{
						/*state = 9;
						retrack();
						return new Token("ASSIGN");*/
						throw new InvalidLessemaException(cursor);
					}
					break;
				case 31:
					state = 32;
					retrack();
					return new Token("EQUAL");
			}

			//Operatori e parentesi
			switch (state){
				case 35:
					if(c== '('){
						lessema += c;
						return installToken("LPAR",null);
					}
					else{
						state = 36;
					}
				break;
			}
			switch (state){
				case 36:
					if(c== ')'){
						lessema += c;
						return installToken("RPAR", null);
					}
					else{
						state = 38;
					}
					break;
			}
			switch (state){
				case 38:
					if(c== '{'){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						return installToken("LBRA", null);
					}
					else{
						state = 39;
					}
					break;
			}
			switch (state){
				case 39:
					if(c== '}'){
						lessema += c;
						return installToken("RBRA", null);
					}
					else{
						state = 40;
					}
					break;
			}
			switch (state){
				case 40:
					if(c== '+'){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						return installToken("PLUS",null);
					}
					else{
						state = 41;
					}
					break;
			}
			switch (state){
				case 41:
					if(c== '-'){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						return installToken("MINUS",null);
					}
					else{
						state = 42;
					}
					break;
			}
			switch (state){
				case 42:
					if(c== '*'){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						return installToken("MUL",null);
					}
					else{
						state = 43;
					}
					break;
			}
			switch (state){
				case 43:
					if(c== '/'){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						return installToken("DIVISION",null);
					}
					else{
						state = 44;
					}
					break;
			}
			switch (state){
				case 44:
					if(c== '<'){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						state = 45;
					}
					else{
						state = 46;
					}
					break;
				case 45:
					if (c == '='){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						return installToken("BIN_OP","LE");
					}
					else if (c == '>'){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						return installToken("BIN_OP","NOT");
					}
					else if (c=='-'){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						state = 56; //STATO AGGIUNTO DOPO
						break;
					}
					else{
						retrack();
						state = 47;
						return installToken("BIN_OP","LT");
					}
			}
			switch (state){
				case 46:
					if(c== '>'){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						state = 47;
					}
					else{
						state = 49;
					}
					break;
				case 47:
					if (c == '='){
						lessema += c;
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						return installToken("BIN_OP","GE");
					}
					else{
						retrack();
						state = 48;
						return installToken("BIN_OP","GT");
					}
			}
			switch (state){
				case 49:
					if(c== ' ') {
						lessema += c;
						return installToken("WHITESPACE", "BLANK");
					}
					else{
						state = 50;
					}
					break;
			}
			switch (state){
				case 50:
					if(c== '\n') {
						lessema += c;
						return installToken("WHITESPACE", "NEW_LINE");
					}
					else{
						state = 52;
					}
					break;
			}
			switch (state){
				case 52:
					if(c== '\t') {
						lessema += c;
						return installToken("WHITESPACE", "TAB");
					}
					else{
						state = 53;
					}
			}
			switch (state){
				case 53:
					if(c== '\r') {
						lessema += c;
						return installToken("WHITESPACE", "NEW_LINE");
					}
					else{
						state = 54;
					}
			}
			switch (state){
				case 54:
					if(c== ',') {
						lessema += c;
						return installToken("SEP", "COMMA");
					}
					else{
						state = 55;
					}
					break;
			}
			switch (state){
				case 55:
					if(c== ';') {
						lessema += c;
						return installToken("SEP", "SEMI");
					}
					else{
						state = 56;
					}
					break;
			}
			switch (state){
				case 56:
					if (c=='-'){
						if(lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						state = 57;
					}
					else{
						state = 58;
					}
					break;
				case 57:
					if (c=='-') {
						if (lookHead == -1) // controlla se è finito il file
							throw new InvalidLessemaException(cursor);
						state = 9;
						return new Token("ASSIGN");
					}
					else{
						throw new InvalidLessemaException(cursor);
					}
			}

		}//end while
	}//end method

	private Token installID(String lessema){
		Token token;
		//utilizzo come chiave della hashmap il lessema
		if(stringTable.containsKey(lessema))
			return stringTable.get(lessema);
		else{
			return installToken("ID", lessema);
		}
	}

	private Token installToken(String name, String value){
		Token token;
		//utilizzo come chiave della hashmap il lessema
		if(stringTable.containsKey(name)){
			if (stringTable.get(name).getAttribute().equals(value))
				return stringTable.get(name);
		}
		token =  new Token(name, value);
		stringTable.put(value, token);
		return token;
	}


	private void retrack(){
		// fa il retract nel file di un carattere
		cursor--;
	}




		
}
