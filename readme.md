# Esercitazione 1 - Hand Coded Lexer


- delimiter -> (blank | tab | newline)+
- letter -> [a-zA-Z]
  
- digit -> [0-9]
  
- number -> digit +((\.digit)|(E(\+|\-)?digit+(\.digit)?))? -> [0-9]+((\.[0-9])|(E(\+|\-)?[0-9]+(\.[0-9])?))?
  
- id -> letter+(number | letter)*
  
- sep -> (\(|\)|\{|\}|,|;)
  
- assign -> <--
  
- equal -> ==
  
- rel_op -> (<= | >= | < | > )
 
  
####Keywords  
- if
- then
- else
- while
- int 
- float